var express = require('express');
const { query, _attributes } = require('../../config/sequelize');
const model = require('./../../model/index');
const { Op, Model } = require('sequelize');


module.exports = async (req, res) => {
    try {

        console.log("subscriber===== ", req.body);
        const data = await model['Subscriber'].create(req.body)
        console.log("data========== ", data)
        if (data) {
            res.status(200).send({
                message: `Thank you ${req.body.firstName}, for subscribing EduOpenings`
            })
        }
        else {
            console.log("creating error============= ")
            const errorData = await model['subscription_failed'].create({
                reason: `This user could not be registered ${req.body.email}  ${req.body.firstName} ${req.body.lastName}`
            })
            throw new Error(errorData)
        }

    }
    catch (err) {
        res.status(406).send({
            message: err.stack
        })
    }
}