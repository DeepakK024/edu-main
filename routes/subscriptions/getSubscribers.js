var express = require('express');
const { query, _attributes } = require('../../config/sequelize');
const model = require('./../../model/index');
const { Op, Model } = require('sequelize');


module.exports = async (req, res) => {
    try {
        const data = await model['Subscriber'].findAndCountAll()
        //data['attributes'] = att
        res.status(200).send({
            message: "All subscribers retrieved succesfully",
            result: {
                data: data
            }
        })
    }
    catch (err) {
        res.status(406).send({
            message: err.stack
        })
    }
}