'use strict'
const { DataTypes } = require('sequelize');
const sequelize = require("../config/sequelize");


const Subscriber = sequelize.define('subscriber', {
    // id:{
    //   type:DataTypes.INTEGER,
    //   autoIncrement:true,
    //   primaryKey:true
    // },
    firstName: {
        type: DataTypes.STRING,
        get() {
            return this.getDataValue('firstName')
        },
        set(value) {
            this.setDataValue('firstName', value);
        }
    },
    lastName: {
        type: DataTypes.STRING,
        get() {
            return this.getDataValue('lastName')
        },
        set(value) {
            this.setDataValue('lastName', value);
        }
    },
    email: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
        unique: {
            msg: 'Email Already registered'
        },
        get() {
            return this.getDataValue('email')
        },
        set(value) {
            this.setDataValue('email', value);
        }
    },
    phone: {
        type: DataTypes.STRING,
        get() {
            return this.getDataValue('phone')
        },
        set(value) {
            this.setDataValue('phone', value);
        }
    },
    organization: {
        type: DataTypes.STRING,
        get() {
            return this.getDataValue('organization')
        },
        set(value) {
            this.setDataValue('organization', value);
        }
    }

}, {
    tableName: 'subscriber'
    //paranoid: true
});


console.log("Subscriber Table", Subscriber === sequelize.models.subscriber);
module.exports = Subscriber;