'use strict'
const { DataTypes } = require('sequelize');
const sequelize = require("../config/sequelize");


const Failure = sequelize.define('failed_subscriptions', {
    // id:{
    //   type:DataTypes.INTEGER,
    //   autoIncrement:true,
    //   primaryKey:true
    // },
    reason: {
        type: DataTypes.STRING,
        get() {
            return this.getDataValue('reason')
        },
        set(value) {
            this.setDataValue('reason', value);
        }
    }

}, {
    tableName: 'failed_subscriptions'
    //paranoid: true
});


console.log("Failed Sunscription Table", Failure === sequelize.models.failed_subscriptions);
module.exports = Failure;